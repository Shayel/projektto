package factory_method;

import PriceChanger.PriceChanger;
import products.Game;
import products.MusicAlbum;
import products.PRODUCTS;
import products.Product;

public class ProductFactory {
	private static ProductFactory _instance = null;
	public static ProductFactory getInstance() {
		if(_instance == null) _instance = new ProductFactory();
		return _instance;
	}
	
	public Product createProduct(PRODUCTS product_id) {
		switch(product_id) {
		case MARIO:
			return new Game(10.f, product_id);
		case BATTLEFIELD:
			Product p = new Game(15.f, product_id);
			PriceChanger changer = new PriceChanger();
			return p;
		case ASTEROIDS:
			return new Game(20.f, product_id);
		case KILL_EM_ALL:
			return new MusicAlbum(20.f, product_id);
		case HIGHWAY_TO_HELL:
			return new MusicAlbum(25.f, product_id);
		default: return null;
		}
	}
}
