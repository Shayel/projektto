package products;

abstract public class Product implements Cloneable {
	protected float price;
	protected PRODUCTS product_type;
	protected int count;
	
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			return null;
		}
	}
	
	public Product() {
		price = 0.0f;
		product_type = null;
		count = 0;
	}
	
	public Product(float price, PRODUCTS type) {
		this.price = price;
		this.product_type = type;
		this.count = 0;
	}
	
	public float getPrice() {
		return price;
	}
	
	public void setPrice(float newPrice) {
		price = newPrice;
	}
		
	public PRODUCTS getType() {
		return product_type;
	}	
	
	public String toString() {
		String tmp = product_type.toString();
		tmp = tmp.replace('_', ' ');
		tmp = tmp.toLowerCase();
		tmp +=  " count: " + count;
		tmp += ", price: " + price;
		tmp = tmp.substring(0,1).toUpperCase() + tmp.substring(1);
		return tmp;
	}
	
	public void addCount(int add) {
		count += add;
	}
	
	public void substractCount(int sub) {
		count -= sub;
	}

	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}

	public String getName() {
		String tmp = product_type.toString();
		tmp = tmp.replace('_', ' ');
		tmp = tmp.toLowerCase();
		tmp = tmp.substring(0,1).toUpperCase() + tmp.substring(1);
		return tmp;
	}
}
