package main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;

import products.Game;
import products.MusicAlbum;
import products.PRODUCTS;
import products.Product;

abstract public class ProductContainer {
	protected Hashtable<PRODUCTS, Product>products;
	public Product getProduct(PRODUCTS type) {
		if(products.containsKey(type)) return (Product) products.get(type).clone();
		return null;
	}

	public Product getSameProduct(PRODUCTS type) {
		if(products.containsKey(type)) return (Product) products.get(type);
		return null;
	}
	
	public boolean contains(PRODUCTS product_type) {
		return products.containsKey(product_type);
	}
	
	public boolean contains(Product prod) {
		return products.contains(prod);
	}
	
	public ProductContainer() {
		products = new Hashtable<PRODUCTS, Product>();
	}
	
	
	public Collection<Product> getProductList() {
		return products.values();
	}
	
	public int getContainerSize() {
		return products.size();
	}
	
	public void addProduct(Product prod, int count) {
		if(!products.containsKey(prod.getType())) {
			prod.setCount(0);
			products.put(prod.getType(), prod);
		}
		products.get(prod.getType()).addCount(count);		
	}
	
	public Product withdrawProduct(int index, int quantity) {
		return null;
	}

	public void substractCount(PRODUCTS product_type, int quantity) {
		products.get(product_type).substractCount(quantity);
		if(products.get(product_type).getCount() == 0) products.remove(product_type);
	}
	
	public void setMemento(Memento memento) {
		memento.restoreState();
	}
	
	public Memento createMemento() {
		Memento mementoToReturn = new Memento();
		mementoToReturn.setState();
		return mementoToReturn;
	}
	
	public class Memento {
		private Hashtable <PRODUCTS, Product> mementoProducts = new Hashtable <PRODUCTS, Product>();
		private void setState() {
			for(Product p : products.values()) {
				mementoProducts.put(p.getType(), (Product) p.clone());
			}
		}
		
		private void restoreState() {
			products = (Hashtable<PRODUCTS, Product>) mementoProducts.clone();
		}
	}

	public ArrayList<Game> getGames() {
		ArrayList<Game> retList = new ArrayList<Game>();
		for(Product p : products.values()) {
			if(p instanceof Game) retList.add((Game)p);
		}
		return retList;
	}

	public ArrayList<MusicAlbum> getMusicAlbums() {
		ArrayList<MusicAlbum> retList = new ArrayList<MusicAlbum>();
		for(Product p : products.values()) {
			if(p instanceof MusicAlbum) retList.add((MusicAlbum)p);
		}
		return retList;
	}

}
