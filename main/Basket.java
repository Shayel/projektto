package main;

import history.History;
import history.HistoryRecord;
import products.Product;

public class Basket extends ProductContainer {
	private History history = new History();
	private float funds = 200.f;
	public void finalizeTransaction() {
		history.addRecord(new HistoryRecord(this.getProductList()));
		funds -= this.getPrice();
		products.clear();
	}
	
	public String getHistoryString() {
		return history.toString();
	}

	public History getHistory() {
		return history;
	}

	public float getFunds() {
		return funds;
	}

	public float getPrice() {
		float price = 0;
		for(Product p : products.values()) {
			price += p.getPrice() * p.getCount();
		}
		return price;
	}
}
