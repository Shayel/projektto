package main;

import java.text.ParseException;
import java.util.Scanner;

import javax.swing.SwingUtilities;

import MVC.BaseController;
import MVC.ConsoleController;
import MVC.SwingMainController;

public class MainClass {
	private static BaseController c;


	public static void main(String[] args) {
		System.out.println("1. Console version\n2. Swing version\n");
		Scanner s = new Scanner(System.in);
		int option = s.nextInt();
		switch(option) {
			case 1:
				c = new ConsoleController();
				c.run();
				break;
			case 2:
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						c = new SwingMainController();
						c.run();
					}
				});
				break;
			default:
				System.exit(0);
		}
	}
}
