package main;


import factory_method.ProductFactory;
import products.PRODUCTS;
import products.Product;

public class Warehouse extends ProductContainer {
	private int initialCount = 5;
	public Warehouse() {
		super();		
		for(PRODUCTS type : PRODUCTS.values()) {
			Product prod = ProductFactory.getInstance().createProduct(type);
			this.addProduct(prod, initialCount);
		}		
	}	
}
