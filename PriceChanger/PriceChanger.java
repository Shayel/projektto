package PriceChanger;

import products.Product;

public class PriceChanger {
	public void changePrice(Product p, float newPrice) {
		p.setPrice(p.getPrice() * newPrice);
	}
}
