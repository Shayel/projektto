package MVC;

/**
 * Created by Shayel on 2015-01-16.
 */
public interface BaseController {
    void run();
}
