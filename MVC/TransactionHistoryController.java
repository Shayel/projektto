package MVC;

import history.HistoryRecord;
import main.ProductContainer;
import main.Warehouse;
import products.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import main.Basket;

/**
 * Created by Shayel on 2015-01-17.
 */
public class TransactionHistoryController {
    private Basket basket;
    private Warehouse warehouse;
    private JDialog view;

    public TransactionHistoryController(Basket b, Warehouse w) {
        this.basket = b;
        this.warehouse = w;
        view = new JDialog();
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.addColumn("Data");
        tableModel.addColumn("Total price");
        tableModel.addColumn("Description");
        final JTable table = new JTable(tableModel);
        table.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                int col = table.columnAtPoint(e.getPoint());
                if (row >= 0 && col == 2) {
                    JDialog d = new JDialog();
                    String description = (String)table.getModel().getValueAt(row, col);
                    description = "<html>" + description + "</html>";
                    description = description.replace(new String("\n"), new String("<br>"));
                    d.add(new JLabel(description));
                    d.pack();
                    d.setVisible(true);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        ArrayList<HistoryRecord>history = basket.getHistory().getRecords();
        Object tab [][] = new Object[history.size()][3];
        for(int i = 0; i<history.size(); i++) {
            tab[i][0] = history.get(i).getDate().toString();
            float price = 0.0f;
            for(Product p : history.get(i).getProducts()) price += p.getPrice() * p.getCount();
            tab[i][1] = price;
            String description = new String();
            for(Product p : history.get(i).getProducts()) description += p.toString() + "\n";
            tab[i][2] = description;
        }
        for(Object[] row : tab) tableModel.addRow(row);
        JScrollPane scrollPane = new JScrollPane(table);
        view.add(scrollPane);
        view.pack();
        view.setVisible(true);
    }
}
