package MVC;

import java.util.Scanner;
import java.util.Stack;

import command.ProductPrinter;
import main.Basket;
import main.ProductContainer;
import main.Warehouse;
import products.PRODUCTS;
import products.Product;
import InputManager.InputManager;
import InputManager.ProcessManager;
import builder.BasketBuilder;
import builder.Creator;
import builder.WarehouseBuilder;

public class ConsoleController implements BaseController {
	private Warehouse warehouse;
	private Basket basket;
	private ConsoleView view = new ConsoleView();
	private ProductPrinter printWarehouseProducts;
	private ProductPrinter printBasketProducts;

	private StateHistory history = new StateHistory();
	private InputManager inputManager;
	private Scanner s;
	public ConsoleController() {
		Creator c = new Creator();
		c.setContainerBuilder(new WarehouseBuilder());
		c.createContainer();
		warehouse = (Warehouse) c.getContainer();
		c.setContainerBuilder(new BasketBuilder());
		c.createContainer();
		basket = (Basket) c.getContainer();
		printBasketProducts = new ProductPrinter(view, basket);
		printWarehouseProducts = new ProductPrinter(view, warehouse);
	}
	
	public void run() {
		s = new Scanner(System.in);
		inputManager = new InputManager();
		inputManager.setProcessManager(new MainMenuManager());
			
		int choice = 0;
		while(true) {
			view.printMainMenu();
			choice = s.nextInt();
			inputManager.processCommand(choice);
		}
	}
	
	private void buyProducts() {
		String type;
		int quantity;
		view.printProductList(warehouse.getProductList());
		view.print("Product to buy:");
		s.nextLine();
		type = s.nextLine();
		view.print("Quantity:");
		quantity = s.nextInt();
		if(quantity <= 0) return;
		type = type.toUpperCase().replace(' ', '_');
		PRODUCTS product_type = PRODUCTS.valueOf(type);
		System.out.println(warehouse.getProduct(product_type).getCount());
		if(warehouse.contains(product_type) && warehouse.getProduct(product_type).getCount() >= quantity) {
			saveHistory();
			Product p = warehouse.getProduct(product_type);
			basket.addProduct(p, quantity);
			warehouse.substractCount(product_type, quantity);
		}
	}

	private void saveHistory() {
		StateHistoryRecord record = new StateHistoryRecord();
		record.basketMemento = basket.createMemento();
		record.warehouseMemento = warehouse.createMemento();
		history.add(record);
	}
	private void recoverHistory() {
		StateHistoryRecord r = history.getLast();
		System.out.println(r);
		if(r == null){
			System.out.println("No history left");
			return;
		}
		basket.setMemento(r.basketMemento);
		warehouse.setMemento(r.warehouseMemento);
	}
	
	private class MainMenuManager implements ProcessManager {
		@Override
		public void processCommand(int command) {
			switch(command) {
			case 1:
				printWarehouseProducts.execute();
				break;
			case 2:
				buyProducts();
				break;
			case 3:
				printBasketProducts.execute();
				view.print("Total value: " + basket.getPrice());
				break;
			case 4:
				basket.finalizeTransaction();
				history.clear();
				break;
			case 5:
				view.print(basket.getHistoryString());
				break;
			case 6:
				recoverHistory();
				break;
			case 0:
				System.exit(0);
				break;
			}		
		}
	}
	
	private class ExitManager implements ProcessManager {

		@Override
		public void processCommand(int choice) {
			view.print("Shop closed!");
			System.exit(0);
		}
		
	}
}

class StateHistoryRecord {
	public ProductContainer.Memento warehouseMemento, basketMemento;
}

class StateHistory {
	private Stack<StateHistoryRecord>history = new Stack<StateHistoryRecord>();
	public StateHistoryRecord getLast() {
		if(!history.isEmpty()) {
			return history.pop();
		}
		return null;
	}
	
	public void add(StateHistoryRecord r) {
		history.push(r);
	}

	public void clear() {
		history.clear();
	}
}
