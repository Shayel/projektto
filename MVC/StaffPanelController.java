package MVC;

import PriceChanger.PriceChanger;
import main.Basket;
import main.ProductContainer;
import main.Warehouse;
import products.PRODUCTS;
import products.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created by Shayel on 2015-01-17.
 */
public class StaffPanelController {
    private JDialog view;
    private JTextField changeValueField = new JTextField();
    private JButton changeButton = new JButton("Change prices!");
    private Basket basket;
    private Warehouse warehouse;
    DefaultListModel<PRODUCTS> listModel = new DefaultListModel<PRODUCTS>();
    JList list = new JList(listModel);

    public StaffPanelController(Basket basket, Warehouse warehouse) {
        this.basket = basket;
        this.warehouse = warehouse;
        view = new JDialog();
        view.setModal(true);
        view.setLayout(new FlowLayout());
        for(PRODUCTS p : PRODUCTS.values()) listModel.addElement(p);
        view.add(new JScrollPane(list));
        changeValueField.setColumns(5);
        changeButton.addActionListener(new ChangePrice());
        view.add(changeValueField);
        changeButton.addActionListener(new ChangePrice());
        view.add(changeButton);
        view.pack();
        view.setVisible(true);
    }

    private boolean isInteger(String str) {
        try
        {
            double d = Integer.parseInt(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
           return true;
    }

    private class ChangePrice implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            PriceChanger changer = new PriceChanger();
            int value = 1;
            if(isInteger(changeValueField.getText())) value = Integer.parseInt(changeValueField.getText());
            int [] selected = list.getSelectedIndices();
            for(int i : selected) {
                PRODUCTS p = listModel.getElementAt(i);
                if(warehouse.getProduct(p) != null) changer.changePrice(warehouse.getSameProduct(p), value);
                if(basket.getProduct(p) != null) changer.changePrice(basket.getSameProduct(p), value);
            }
        }
    }

}
