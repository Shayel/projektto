package MVC;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.MaskFormatter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import builder.BasketBuilder;
import builder.Creator;
import builder.WarehouseBuilder;
import command.GameGetter;
import command.MusicAlbumsGetter;
import command.ProductGetter;
import history.History;
import history.HistoryRecord;
import main.Basket;
import main.ProductContainer;
import main.Warehouse;
import products.PRODUCTS;
import products.Product;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.ArrayList;

public class SwingMainController implements BaseController{
	private JFrame view;
	private Warehouse warehouse;
	private Basket basket;

	private JLabel title = new JLabel("Sklep - projekt zaliczeniowy z Technologii Obiektowych"), selectedProductLabel = new JLabel(), totalPriceLabel = new JLabel("Sum: 0"), totalFundsLabel= new JLabel("Funds left: ");
	private SpinnerNumberModel countModel, productCountModel;
	private JTree tree;
	private DefaultMutableTreeNode root, musicProduct, musicAlbum, games;
	private JButton buyButton = new JButton("Buy!"), backButton = new JButton("Step back!"), finalizeButton = new JButton("Finish"),
			removeButton = new JButton("Remove!"), transactionHistory = new JButton("Transaction history"), staffPanelButton = new JButton("Staff panel");
	private Product selectedProduct;
	private JList basketList;
	private DefaultListModel<Product> basketListModel;
	private JTable productTable;
	private DefaultTableModel productTableModel;
	private StateHistory history = new StateHistory();

	public SwingMainController() {
		initializeModels();
		view = new JFrame();
		view.setLayout(new BorderLayout());
		view.setSize(850, 400);
		view.setResizable(false);
		view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		view.add(title, BorderLayout.PAGE_START);
		//LEFT SIDE
		initializeTree();
		JPanel p = new JPanel();
		p.setLayout(new BorderLayout());
		p.add(tree, BorderLayout.PAGE_START);
		staffPanelButton.addActionListener(new StaffPanel());
		p.add(staffPanelButton, BorderLayout.PAGE_END);
		view.add(p, BorderLayout.LINE_START);
		/////

		//CENTER
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		JPanel tablePanel = new JPanel();
		tablePanel.setLayout(new BorderLayout());
		initializeTable();
		JScrollPane tableScrollPane = new JScrollPane(productTable);
		tableScrollPane.setPreferredSize(new Dimension(350, 150));
		tablePanel.add(new JLabel("Products"), BorderLayout.PAGE_START);
		tablePanel.add(tableScrollPane, BorderLayout.CENTER);
		JPanel bottom = new JPanel();
		countModel = new SpinnerNumberModel(0,0,10,1);
		bottom.add(selectedProductLabel);
		bottom.add(new JSpinner(countModel));
		buyButton.addActionListener(new BuyProducts());
		bottom.add(buyButton);
		backButton.addActionListener(new StepBack());
		bottom.add(backButton);
		tablePanel.add(bottom, BorderLayout.PAGE_END);
		centerPanel.add(tablePanel);
		view.add(centerPanel, BorderLayout.CENTER);
		////

		//RIGHT SIDE
		initializeBasket();
	}

	private void initializeTable() {
		productTableModel = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int cell) {
				return false;
			}
		};
		productTableModel.addColumn("Name");
		productTableModel.addColumn("Quantity");
		productTableModel.addColumn("Price");
		productTable = new JTable(productTableModel);
		productTable.getSelectionModel().addListSelectionListener(new SelectProduct());
	}

	private void initializeBasket() {
		basketListModel = new DefaultListModel<Product>();
		basketList = new JList(basketListModel);
		basketList.setPreferredSize(new Dimension(200,500));
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.PAGE_AXIS));
		rightPanel.add(new JLabel("Basket"));
		JScrollPane scrollPane= new JScrollPane(basketList);
		rightPanel.add(scrollPane);
		JPanel basketBottom = new JPanel();
		basketBottom.setPreferredSize(new Dimension(350, rightPanel.getWidth()));
		basketBottom.add(totalPriceLabel);
		finalizeButton.addActionListener(new Finalize());
		basketBottom.add(finalizeButton);
		productCountModel = new SpinnerNumberModel(0, 0, 1000, 1);
		basketBottom.add(new JSpinner(productCountModel));
		removeButton.addActionListener(new RemoveProducts());
		basketBottom.add(removeButton);
		totalFundsLabel.setText("Funds left: " + basket.getFunds());
		basketBottom.add(totalFundsLabel);
		rightPanel.add(basketBottom);
		transactionHistory.addActionListener(new TransactionHistory());
		rightPanel.add(transactionHistory);
		view.add(rightPanel, BorderLayout.LINE_END);
	}

	private void initializeModels() {
		Creator c = new Creator();
		c.setContainerBuilder(new WarehouseBuilder());
		c.createContainer();
		warehouse = (Warehouse) c.getContainer();
		c.setContainerBuilder(new BasketBuilder());
		c.createContainer();
		basket = (Basket) c.getContainer();
	}

	private void initializeTree() {
		root = new DefaultMutableTreeNode("Produkty");
		musicProduct = new DefaultMutableTreeNode("Music Products");
		musicAlbum = new DefaultMutableTreeNode("Music albums");
		games = new DefaultMutableTreeNode("Games");
		musicProduct.add(musicAlbum);
		root.add(musicProduct);
		root.add(games);
		tree = new JTree(root);
		Color bgColor = new Color(240,240,240);
		tree.setBackground(bgColor);
		DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) tree.getCellRenderer();
		renderer.setBackgroundNonSelectionColor(bgColor);
		tree.getSelectionModel().addTreeSelectionListener(new DisplayProducts());
	}

	private class DisplayProducts implements TreeSelectionListener {

		@Override
		public void valueChanged(TreeSelectionEvent e) {
			insertProductsIntoTable();
		}
	}

	private class SelectProduct implements ListSelectionListener {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			if(e.getValueIsAdjusting()) return;
			int rowNum = productTable.getSelectedRow();
			if(rowNum<0) return;
			String name = productTable.getValueAt(productTable.getSelectedRow(), 0).toString();
			selectedProductLabel.setText(name);
			name = name.toUpperCase();
			name = name.replace(' ', '_');
			System.out.println(name);
			selectedProduct = warehouse.getProduct(PRODUCTS.valueOf(name));
			countModel.setMaximum(new Integer((Integer)productTable.getValueAt(productTable.getSelectedRow(), 1)));
			countModel.setValue(new Integer(0));
		}
	}

	private void insertProductsIntoTable() {
		ProductGetter getter;
		DefaultMutableTreeNode lastNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

		if(lastNode == null || !lastNode.isLeaf()) return;
		String selection = lastNode.getUserObject().toString();
		if(selection == "Games") getter = new GameGetter(warehouse);
		else getter = new MusicAlbumsGetter(warehouse);
		final ArrayList<Product> products = (ArrayList<Product>) getter.getProducts();
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				productTableModel.setRowCount(0);
				ArrayList<Object> values = new ArrayList<Object>();
				for (Product p : products) {
					values.add(p.getName());
					values.add(p.getCount());
					values.add(p.getPrice());
					productTableModel.addRow(values.toArray());
					values.clear();
				}
			}
		});
	}

	private void insertProductsIntoBasket() {
		basketListModel.clear();
		for(Product p : basket.getProductList()) {
			basketListModel.addElement(p);
		}
		float sum = basket.getPrice();
		totalPriceLabel.setText("Sum: "+ sum);
		float funds = basket.getFunds();
		totalFundsLabel.setText("Funds left: " + funds);
	}

	private class Finalize implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(basket.getPrice() > basket.getFunds()) {
				JOptionPane.showMessageDialog(view, "Not enough money!");
				return;
			}
			basket.finalizeTransaction();
			history.clear();
			insertProductsIntoBasket();
			totalFundsLabel.setText("Funds left: " + basket.getFunds());
		}
	}

	private class RemoveProducts implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if(basketList.getSelectedIndex() < 0) return;
			Product selectedProduct = basketListModel.getElementAt(basketList.getSelectedIndex());
			PRODUCTS type = selectedProduct.getType();
			int count = (Integer)productCountModel.getValue();
			if(count == 0) return;
			if(count > selectedProduct.getCount()) count = selectedProduct.getCount();
			saveHistory();
			transferProducts(basket, warehouse, type, count);
			insertProductsIntoTable();
			insertProductsIntoBasket();
		}
	}

	private class BuyProducts implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if(selectedProduct == null) return;
			PRODUCTS type = selectedProduct.getType();
			int count = countModel.getNumber().intValue();
			if(count == 0 || warehouse.getProduct(type).getCount() < count) return;
			saveHistory();
			transferProducts(warehouse, basket, type, count);
			insertProductsIntoTable();
			insertProductsIntoBasket();
		}
	}

	private void transferProducts(ProductContainer from, ProductContainer to, PRODUCTS type, int count) {
		Product p = from.getProduct(type);
		to.addProduct(p, count);
		from.substractCount(type, count);
	}

	private class StaffPanel implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new StaffPanelController(basket, warehouse);
			insertProductsIntoBasket();
			insertProductsIntoTable();
		}
	}

	private class TransactionHistory implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			new TransactionHistoryController(basket, warehouse);
		}
	}

	private class StepBack implements  ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			recoverHistory();
			insertProductsIntoTable();
			insertProductsIntoBasket();
		}
	}
	private void saveHistory() {
		StateHistoryRecord record = new StateHistoryRecord();
		record.basketMemento = basket.createMemento();
		record.warehouseMemento = warehouse.createMemento();
		history.add(record);
	}
	private void recoverHistory() {
		StateHistoryRecord r = history.getLast();
		if(r == null){
			return;
		}
		basket.setMemento(r.basketMemento);
		warehouse.setMemento(r.warehouseMemento);
	}
	public void run() {
		view.setVisible(true);
	}
}
