package MVC;

import java.util.Collection;

import products.Product;

public class ConsoleView {
	private String mainMenu = "1. List warehouse products\n"
			+ "2. Buy product\n"
			+ "3. List my products\n"
			+ "4. Finalize transaction\n"
			+ "5. Print history\n" 
			+ "6. Step back \n"			
			+ "0. Exit";
	
	public void printProductList(Collection<Product>products) {
		for(Product p : products) System.out.println(p);
	}
	
	public void printMainMenu() {
		System.out.println(mainMenu);
	}
	
	public void print(Object txt) {
		System.out.println(txt);
	}
}
