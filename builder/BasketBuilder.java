package builder;

import main.Basket;

public class BasketBuilder extends ContainerBuilder {

	@Override
	public void createContainer() {
		container = new Basket();
	}

}
