package builder;

import main.ProductContainer;
import products.Product;

public class Creator {
	private ContainerBuilder builder;
	public void setContainerBuilder(ContainerBuilder b) {
		builder = b;
	}
	public ProductContainer getContainer() {
		return builder.getContainer();
	}
	public void createContainer() {
		builder.createContainer();
	}
}
