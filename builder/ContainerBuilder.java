package builder;

import main.ProductContainer;
import products.Product;

public abstract class ContainerBuilder {
	protected ProductContainer container;
	public ProductContainer getContainer() {
		return container;
	}
	
	public abstract void createContainer();
}
