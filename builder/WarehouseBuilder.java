package builder;

import main.Warehouse;

public class WarehouseBuilder extends ContainerBuilder {

	@Override
	public void createContainer() {
		container = new Warehouse();
	}

}
