package command;

import main.ProductContainer;

import java.util.Collection;

/**
 * Created by Shayel on 2015-01-15.
 */
public abstract class ProductGetter {
    protected ProductContainer container;
    public ProductGetter(ProductContainer container) {
        this.container = container;
    }

    public abstract Collection getProducts();
}
