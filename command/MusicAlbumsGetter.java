package command;

import main.ProductContainer;

import java.util.Collection;

/**
 * Created by Shayel on 2015-01-15.
 */
public class MusicAlbumsGetter extends ProductGetter{

    public MusicAlbumsGetter(ProductContainer c) {
        super(c);
    }
    @Override
    public Collection getProducts() {
        return container.getMusicAlbums();
    }
}
