package command;

import main.ProductContainer;

import java.util.Collection;

/**
 * Created by Shayel on 2015-01-15.
 */
public class GameGetter extends ProductGetter {
    public GameGetter(ProductContainer c) {
        super(c);
    }
    @Override
    public Collection getProducts() {
        return container.getGames();
    }
}
