package command;

import javax.swing.text.View;

import main.ProductContainer;
import MVC.ConsoleView;

public class ProductPrinter implements Command {
	private ConsoleView view;
	private ProductContainer container;
	public ProductPrinter(ConsoleView view, ProductContainer container) {
		this.view = view;
		this.container = container;
	}
	public void execute() {
		view.printProductList(container.getProductList());
	}
}
