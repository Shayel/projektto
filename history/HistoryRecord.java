package history;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import products.Product;

public class HistoryRecord {
	private Date date;
	private Collection<Product>products;
	
	public HistoryRecord(Collection<Product> collection) {
		//this.products = (Collection<Product>) collection;
		products = new ArrayList<Product>();
		for(Product p : collection) products.add(p);
		date = new Date();
	}
	
	public String toString() {
		String string = new String();
		string += date.toString() + '\n';
		for(Product p : products) {
			string += p.toString() + '\n';
		}
		return string;
	}

	public Date getDate() {
		return date;
	}

	public ArrayList<Product> getProducts() {
		return (ArrayList<Product>) products;
	}
}
