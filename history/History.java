package history;

import java.util.ArrayList;
import java.util.Collection;

public class History {
	private Collection<HistoryRecord>records = new ArrayList<HistoryRecord>();
	
	public void addRecord(HistoryRecord r) {
		records.add(r);
	}
	
	public String toString() {
		String string = new String();
		for(HistoryRecord r : records) {
			string += r.toString();
		}
		return string;
	}

	public ArrayList<HistoryRecord> getRecords() {
		return (ArrayList<HistoryRecord>) records;
	}
}
