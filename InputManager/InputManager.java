package InputManager;

public class InputManager {
	private ProcessManager manager;
	public void setProcessManager(ProcessManager manager) {
		this.manager = manager;
	}
	
	public void processCommand(int choice) {
		manager.processCommand(choice);
	}
}
